package org.magmacollective.darkcrystal.secretsharingwrapper;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.magmacollective.darkcrystal.secretsharingwrapper.SecretV1.Secret;
import org.magmacollective.darkcrystal.keybackup.crypto.EdDSA;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;
import org.magmacollective.libsss.SSS;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * Wrapper around 'sss' secrets library, providing extra features
 * Dark Crystal Distributed Key Backup
 * @author Magma Collective
 */
public class SecretSharingWrapper {
  public static final int KEYSHARE_LEN = SSS.KEYSHARE_LEN;
  public static final int MAX_SHARES = 255;
  public static final int SIGNATURE_LEN = EdDSA.SIGNATURE_LENGTH;
  public static final int MAC_LEN = KeyBackupCrypto.MAC_BYTES;
  public static final int NONCE_LEN = KeyBackupCrypto.NONCE_BYTES;

  private SecretSharingWrapper() {
  }

  /**
   * Class for packing a secret together with a descriptive label
   */
  public static class SecretWithLabel {
    private byte[] secret;
    private String label;

    /**
     * Constructor takes a secret and a label
     * @param s the secret
     * @param l a descriptive label
     */
    SecretWithLabel(byte[] s, String l) {
      secret = s;
      label = l;
    }

    /**
     * Encode the message to a byte array using protocol buffers
     * @return the encoded message containing both secret and label
     */
    public byte[] encode() {
      return Secret.newBuilder()
              .setSecret(ByteString.copyFrom(secret))
              .setLabel(label)
              .build()
              .toByteArray();
    }

    /**
     * Getter method for secret
     * @return the secret given in the constructor
     */
    public byte[] getSecret() {
      return secret;
    }

    /**
     * Getter method for label
     * @return the label given in the constructor
     */
    public String getLabel() {
      return label;
    }
  }

  /**
   * Decode a packed secret
   * @param packedSecret A secret message previously encoded with `secretWithLabel.encode()`
   * @return a secret object
   * @throws InvalidProtocolBufferException If there was a problem decoding
   */
  public static SecretWithLabel decodeSecretWithLabel(byte[] packedSecret) throws InvalidProtocolBufferException {
    Secret secretParsed = Secret.parseFrom(packedSecret);
    return new SecretWithLabel(secretParsed.getSecret().toByteArray(), secretParsed.getLabel());
  }

  /**
   * Partially shuffle a list of byte arrays, randomly selecting the given number of entries
   * This is used to randomise the index value of the shares
   * @param list to be shuffled
   * @param elementsToTake the number of entries to randomly select
   * @return a random subset of the given list
   */
  public static List<byte[]> partialShuffleList(List<byte[]> list, int elementsToTake)
  {
    final SecureRandom random = new SecureRandom();
    final List<byte[]> shuffledPartialList = new ArrayList<byte[]>();
    for (int i = list.size() - 1; i >= list.size() - elementsToTake; i--)
    {
      int index = random.nextInt(i + 1);
      shuffledPartialList.add(list.get(index));
      list.set(index, list.get(i));
    }
    return shuffledPartialList;
  }

  /**
   * An internal method to get the maximum number of shares and return a random subset
   * @param secret
   * @param n desired number of shares
   * @param k threshold
   * @return List of shares
   */
  private static List<byte[]> shareFixedLength(byte[] secret, int n, int k) {
    List<byte[]> allShares = SSS.createKeyshares(secret, MAX_SHARES, k);
    return partialShuffleList(allShares, n);
  }

  /**
   * Create shares for a secret of any length
   * @param secret
   * @param n the number of shares to create
   * @param k the threshold
   * @return a list of shares
   */
  public static List<byte[]> share(byte[] secret, int n, int k) {
    byte[] key = KeyBackupCrypto.generateSymmetricKey();
    byte[] cipherText = KeyBackupCrypto.encrypt(secret, key);
    List<byte[]> shares = shareFixedLength(key, n, k);
    List<byte[]> sharesWithCipherText = new ArrayList<byte[]>();
    for (byte[] share : shares) {
      sharesWithCipherText.add(KeyBackupCrypto.byteArrayConcat(share, cipherText));
    };
    return sharesWithCipherText;
  }

  /**
   * Attempt to recover the secret by combining shares created with the share method
   * @param shares a list of shares
   * @return the secret, if successful
   */
  public static byte[] combine(List<byte[]> shares) throws GeneralSecurityException {
    HashSet<ByteBuffer> cipherTexts = new HashSet<>();
    List<byte[]> keyShares = new ArrayList<>();
    for (byte[] share : shares) {
      byte[] keyShare = new byte[KEYSHARE_LEN];
      System.arraycopy(share, 0, keyShare, 0, KEYSHARE_LEN);
      keyShares.add(keyShare);

      byte[] cipherText = new byte[share.length - KEYSHARE_LEN];
      System.arraycopy(share, KEYSHARE_LEN, cipherText, 0, cipherText.length);
      cipherTexts.add(ByteBuffer.wrap(cipherText));
    }
    byte[] key = SSS.combineKeyshares(keyShares);

    // Assert that all ciphertexts are identical
    // TODO make this a only warning - we should be able to anyway attempt to decrypt them
    if (cipherTexts.size() != 1)
      throw new GeneralSecurityException("Found shares with differing ciphertexts");

    ByteBuffer cipherTextByteBuffer = cipherTexts.iterator().next();
    byte[] cipherText = new byte[cipherTextByteBuffer.remaining()];
    cipherTextByteBuffer.get(cipherText);
    return KeyBackupCrypto.decrypt(cipherText, key);
  }

  /**
   * A convenience method to create shares and sign them with a given secret key
   * @param secret
   * @param n the number of shares
   * @param k the threshold
   * @param privateKey an EdDSA signing private key
   * @return a list of signed shares
   */
  public static List<byte[]> shareAndSign(byte[] secret, int n, int k, PrivateKey privateKey) throws Exception {
    List<byte[]> rawShares = share(secret, n, k);
    List<byte[]> signedShares = new ArrayList<byte[]>();
    EdDSA edDSA = new EdDSA();
    edDSA.signMessage(secret, privateKey);
    for (byte[] share : rawShares) {
      signedShares.add(edDSA.signMessage(share, privateKey));
    };
    return signedShares;
  }

  /**
   * Convenience method to verify a list of shares containing signates and combine them if
   * successful.
   * @param signedShares a list of shares containing signatures
   * @param publicKey an EdDSA public signing key
   * @return the secret, if successful
   * @throws Exception
   */
  public static byte[] verifyAndCombine(List<byte[]> signedShares, PublicKey publicKey)
          throws Exception {
    EdDSA edDSA = new EdDSA();
    List<byte[]> rawShares = new ArrayList<>();
    for (byte[] signedShare : signedShares) {
      if (!edDSA.verifyMessage(signedShare, publicKey))
        throw new GeneralSecurityException("Unable to verify share");
      rawShares.add(EdDSA.detachMessage(signedShare));
    }
    return combine(rawShares);
  }

  /**
   * Validate whether a given byte array could be a shard by checking its length
   * @param share a byte array which may be a shard
   * @return true if the length is ok
   */
  public static boolean isShare(byte[] share) {
    return share.length > KEYSHARE_LEN + MAC_LEN + NONCE_LEN;
  }

  /**
   * Validate whether a given byte array could be a signed share by checking its length
   * @param signedShare
   * @return true if the length is ok
   */
  public static boolean isSignedShare(byte[] signedShare) {
    return signedShare.length > KEYSHARE_LEN + MAC_LEN + NONCE_LEN + SIGNATURE_LEN;
  }

  /**
   * Pad the secret with zeros to the given length.
   * If the secret is longer than the given length, it will pad to the nearest
   * multiple of the given length.
   * @param input the secret of any length
   * @param padLength the desired length
   * @return the zero padded result
   */
  public static byte[] zeroPad(byte[] input, int padLength) {
    byte[] output = new byte[(padLength * (input.length / padLength)) + padLength];
    System.arraycopy(input, 0, output, 0, input.length);
    return output;
  }

  /**
   * Give a recommended threshold value, given the number of custodians
   * @param numberCustodians
   * @return a recommended threshold value
   */
  public static int defaultThreshold(int numberCustodians) {
    return (int)(numberCustodians * 0.75f);
  }

  /**
   * Gives a threshold sanity factor, given a threshold and number of custodians
   * 0 is ideal.  Positive values represent the level of danger of
   * loosing access to the secret.
   * Negative values represent the level of danger of an attacker gaining the secret.
   * @param threshold
   * @param numberCustodians
   */
  public static int thresholdSanity(int threshold, int numberCustodians) {
    return threshold - defaultThreshold(numberCustodians);
  }
}
