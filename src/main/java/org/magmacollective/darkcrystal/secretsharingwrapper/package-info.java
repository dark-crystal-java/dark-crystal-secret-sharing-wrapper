/**
 * A wrapper around the secret sharing library providing higher level functionality
 * This includes variable length shares, obfuscation of the x coordinate, and
 * signing and verification of shares
 */
package org.magmacollective.darkcrystal.secretsharingwrapper;
