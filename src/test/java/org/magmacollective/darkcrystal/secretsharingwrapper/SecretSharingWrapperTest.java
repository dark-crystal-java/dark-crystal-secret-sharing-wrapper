package org.magmacollective.darkcrystal.secretsharingwrapper;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.combine;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.isShare;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.isSignedShare;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.share;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.shareAndSign;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.verifyAndCombine;
import static org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper.zeroPad;

import com.google.protobuf.InvalidProtocolBufferException;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.magmacollective.darkcrystal.keybackup.crypto.EdDSA;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;
import org.magmacollective.libsss.SSS;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Tests for the secret sharing wrapper
 *
 * @author Magma Collective
 */
public class SecretSharingWrapperTest {

    /**
     * Test whether all elements of a list are distinct
     *
     * @param someList
     * @return true if there are no duplicate values
     */
    private static boolean areDistinct(List<byte[]> someList) {
        Set<byte[]> s = new HashSet<byte[]>(someList);
        return (s.size() == someList.size());
    }

    /**
     * Basic test for encryption
     *
     * @throws GeneralSecurityException
     */
    @Test
    @DisplayName("Test encryption")
    public void testEncryption() throws GeneralSecurityException {
        final String message = "beep boop";
        KeyBackupCrypto crypto = new KeyBackupCrypto();
        final byte[] key = crypto.generateSymmetricKey();
        final byte[] ciphertext = crypto.encrypt(message.getBytes(), key);

        assertFalse(Arrays.equals(ciphertext, message.getBytes()));
        final byte[] plaintext = crypto.decrypt(ciphertext, key);
        String plaintextString = new String(plaintext, StandardCharsets.UTF_8);
        assertTrue(plaintextString.equals(message));
    }

    @Test
    @DisplayName("Partial Shuffle List")
    public void testShuffleList() {
        // SecretSharingWrapper secret = new SecretSharingWrapper(); // TODO not needed
        final List<byte[]> list = new ArrayList<byte[]>();
        list.add("1...".getBytes());
        list.add("2...".getBytes());
        list.add("3...".getBytes());
        list.add("4...".getBytes());
        list.add("5...".getBytes());
        final List<byte[]> partialList = SecretSharingWrapper.partialShuffleList(list, 3);
        assertTrue(areDistinct(partialList), "Partial list contains no duplicate elements");
        assertTrue(partialList.size() == 3, "Partial list has correct length");
    }

    @Test
    @DisplayName("Pack secret with label")
    public void testPackSecretWithLabel() {
        // SecretSharingWrapper secret = new SecretSharingWrapper();
        final byte[] encodedSecret = new SecretSharingWrapper.SecretWithLabel("some secret".getBytes(),
                "label for secret").encode();
        try {
            SecretSharingWrapper.SecretWithLabel secretWithLabel = SecretSharingWrapper.decodeSecretWithLabel(encodedSecret);
            assertTrue(secretWithLabel.getLabel().equals("label for secret"), "Label is correct after decoding");
            assertTrue(new String(secretWithLabel.getSecret()).equals("some secret"), "Secret is correct after decoding");
        } catch (InvalidProtocolBufferException e) {
            System.out.println(e.getMessage()); // TODO assert no error
        }
    }

    @Test
    @DisplayName("SSS createShares, combineShares")
    public void testSSS() {
        byte[] secret = "this is 32 bytes really for surethis is 32 bytes really for sure".getBytes();
        List<byte[]> shares = SSS.createShares(secret, 5, 3);
        byte[] reconstructed = SSS.combineShares(shares);
        assertTrue(Arrays.equals(secret, reconstructed));
    }

    @Test
    @DisplayName("SSS createKeyShares, combineKeyShares")
    public void testSSSKeyShares() {
        byte[] secret = "this is 32 bytes really for sure".getBytes();
        List<byte[]> shares = SSS.createKeyshares(secret, 5, 3);
        byte[] reconstructed = SSS.combineKeyshares(shares);
        assertTrue(Arrays.equals(secret, reconstructed), "Secret correctly recovered");
    }

    /**
     * Test basic sharing and recombining
     *
     * @throws GeneralSecurityException
     */
    @Test
    @DisplayName("Wrapper: share, combine")
    public void testSecretsWrapper() throws GeneralSecurityException {
        byte[] secret = "length of this message is not important".getBytes();
        List<byte[]> shares = share(secret, 5, 3);
        byte[] reconstructed = combine(shares);

        for (byte[] share : shares) {
            assertTrue(isShare(share), "Share has correct length");
        }
        ;

        assertTrue(Arrays.equals(secret, reconstructed), "Secret correctly recovered");

        shares.remove(0);
        shares.remove(0);
        byte[] reconstructedThreshold = combine(shares);
        assertTrue(Arrays.equals(secret, reconstructedThreshold), "Secret correctly recovered");
    }

    /**
     * Test that sharing and combining fails on not meeting the theshold number of shares
     */
    @Test
    @DisplayName("Wrapper: share, combine - fails")
    public void testSecretsWrapperFails() {
        byte[] secret = "length of this message is not important".getBytes();
        List<byte[]> shares = share(secret, 5, 3);
        shares.remove(0);
        shares.remove(0);
        shares.remove(0);
        assertThrows(GeneralSecurityException.class, () -> {
            combine(shares);
        });
    }

    /**
     * Test signing and verification of shares
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Wrapper: shareAndSign, verifyAndCombine")
    public void testShareAndSignVerifyAndCombine() throws Exception {
        byte[] secret = "its nice to be important but its more important to be nice".getBytes();
        EdDSA edDSA = new EdDSA();
        KeyPair keyPair = edDSA.generateKeyPair();
        List<byte[]> signedShares = shareAndSign(secret, 5, 3, keyPair.getPrivate());

        for (byte[] signedShare : signedShares) {
            assertTrue(isSignedShare(signedShare), "Share has correct length");
        }
        ;

        signedShares.remove(0);
        signedShares.remove(0);
        assertArrayEquals(secret, verifyAndCombine(signedShares, keyPair.getPublic()));
    }

    /**
     * Tests zero padding of secrets
     */
    @Test
    @DisplayName("Zero padding of secret")
    public void testZeroPadding() {
        byte[] secret = "this has a length of 23".getBytes();
        byte[] paddedSecret = zeroPad(secret, 32);
        assertTrue(paddedSecret.length == 32, "Secret padded to correct length");
        byte[] shouldBeZeros = new byte[32 - secret.length];
        System.arraycopy(paddedSecret, secret.length, shouldBeZeros, 0, 32 - secret.length);
        assertArrayEquals(shouldBeZeros, new byte[shouldBeZeros.length], "Padding contains 0x00");
    }

    /**
     * Tests zero padding of secrets when padding less than secret length
     */
    @Test
    @DisplayName("Zero padding of secret")
    public void testZeroPaddingOverfill() {
        byte[] secret = "this has a length of 23".getBytes();
        byte[] paddedSecret = zeroPad(secret, 20);
        assertTrue(paddedSecret.length == 40, "Secret padded to correct length");
        byte[] shouldBeZeros = new byte[40 - secret.length];
        System.arraycopy(paddedSecret, secret.length, shouldBeZeros, 0, 40 - secret.length);
        assertArrayEquals(shouldBeZeros, new byte[shouldBeZeros.length], "Padding contains 0x00");
    }
}
