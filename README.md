# Dark Crystal Secret Sharing Wrapper

This provides extra high level functionality to the secret sharing implementation [dark-crystal-shamir-secret-sharing](https://gitlab.com/dark-crystal-java/dark-crystal-shamir-secret-sharing), which is provides Java bindings to the C library [dsprenkels/sss](https://github.com/dsprenkels/sss).

- [API Documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper)

Functionality provided by this package includes:
- Variable length secrets
- Obfuscation of share-index (x coordinate)
- Optional method to pack the secret together with a descriptive label
- Optional method to zero-pad the secret to a fixed length
- Method to validate whether a given byte array could be a share.

These techniques are discussed in more detail in the [Dark Crystal Key Backup Protocol Specification](https://gitlab.com/dark-crystal-java/key-backup-protocol-specification)

See also:
- [dark-crystal-key-backup-crypto](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java) - cryptographic functions used by this package.

### Example usage:

```java
import org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper;
import java.util.List;

public class SecretShare {

  public static void main () {
    byte[] secret = "This is the secret".getBytes();
    String label = "This is a descriptive label for the secret";
    byte[] secretWithLabel = new SecretSharingWrapper.SecretWithLabel(secret, label).encode()
    List<byte[]> shares = SecretSharingWrapper.share(secretWithLabel, 5, 3);

    // Remove two shares to demonstrate we only need the threshold amount (3 shares):
    shares.remove(0);
    shares.remove(0);

    // Recombine the shares:
    SecretSharingWrapper.SecretWithLabel reconstructedSecretWithLabel = SecretSharingWrapper.decodeSecretWithLabel(SecretSharingWrapper.combine(shares));

    System.out.println("Label: " + reconstructedSecretWithLabel.getLabel());
    System.out.println("Secret: " + new String(reconstructedSecretWithLabel.getSecret()));
  }
}
```

## Publishing to Maven

To upload the to Maven, prepare as follows.

Add the following content to your `local.properties` file:

    local.maven.publishing.dir=file:///path/to/maven/repo

In file `build.gradle`, adapt values in the `ext` stanza. The coordinates of
the Maven artifact are defined in file `gradle.properties`.
Increment the `pVersion` variable when uploading a new version.

Then run:

    ./gradlew clean publish

Afterwards commit changes to the Maven repo git and publish to the Maven
server.
